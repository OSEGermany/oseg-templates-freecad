## OSEG - FreeCAD Tech-Draw templates

This repository contains [FreeCAD](https://www.freecadweb.org) templates
to be used for [Open Source Ecology Germany (OSEG)](https://opensourceecology.de/)
tech-drawing exports from FreeCAD.
These are simply SVG files that contain a border
and a box of "meta-data" about the project/drawing,
as one commonly knows them from tech drawings.
That box is usually in the bottom-right corner of the drawing.

You may direct FreeCAD to these files, and so use them directly from within;
no other tool is required.

To modify these templates, we strongly recommend to use [Inkscape](https://inkscape.org).
